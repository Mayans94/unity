﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fader : MonoBehaviour {

    public float FadeTime;

    private Image Panelimage;
    private Color Colour = Color.black;

	// Use this for initialization
	void Start () {
        Panelimage = GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Time.timeSinceLevelLoad < FadeTime)
        {
            float alphaChange = Time.deltaTime / FadeTime;
            Colour.a -= alphaChange;
            Panelimage.color = Colour;
        }
        else
        {
            gameObject.SetActive(false);
        }
	}
}
