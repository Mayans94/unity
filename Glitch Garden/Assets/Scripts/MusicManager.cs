﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour {

    private AudioSource audioSource;
    public AudioClip [] levelMusicChangeArray;

	// Use this for initialization
	void Awake () {
        DontDestroyOnLoad(gameObject);
	}

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void ChangeVolume(float volume)
    {
        audioSource.volume = volume;
    }

    // Update is called once per frame
    private void OnLevelWasLoaded(int level)
    {
        AudioClip audio = levelMusicChangeArray[level];

        if(audio) // if audio has a source
        {
            audioSource.clip = audio;
            audioSource.loop = true;
            audioSource.Play();
        }
    }
}
