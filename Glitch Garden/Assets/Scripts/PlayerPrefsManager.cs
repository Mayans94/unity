﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPrefsManager : MonoBehaviour {

    const string MASTER_VOLUME_KEY = "master_volume";
    const string DIFFICULTY_KEY = "difficulty";
    const string LEVEL_UNLOCK_KEY = "level_unlocked_";

    //VOLUME
    public static void SetMasterVolume(float volume)
    {
        if (volume >= 0f && volume <= 1f)
        {
            PlayerPrefs.SetFloat(MASTER_VOLUME_KEY, volume);
        }
        else
        {
            Debug.LogError("Volume set out of range");
        }
    }

    public static float GetMasterVolume()
    {
        if (PlayerPrefs.HasKey(MASTER_VOLUME_KEY))
        {
            return PlayerPrefs.GetFloat(MASTER_VOLUME_KEY);
        }
        else
        {
            Debug.LogError("Volume not set");
            return 0;
        }
        
    }

    //DIFFICULTY
    public static void SetDifficulty(float difficulty)
    {
        if(difficulty >= 1 && difficulty <= 3)
        {
            PlayerPrefs.SetFloat(DIFFICULTY_KEY, difficulty);
        }
        else
        {
            Debug.LogError("Difficulty set out of range");
        }
    }

    public static float GetDifficulty()
    {
        if (PlayerPrefs.HasKey(DIFFICULTY_KEY))
        {
            return PlayerPrefs.GetFloat(DIFFICULTY_KEY);
        }
        else
        {
            Debug.LogError("Difficulty not set");
            return 1f;   
        }
    }

    //LEVELS
    public static void UnlockLevel(int level)
    {
        if(level <= Application.levelCount - 1)
        {
            PlayerPrefs.SetInt(LEVEL_UNLOCK_KEY + level.ToString(), 1); // 1 for true 0 for false;
        }
        else
        {
            Debug.LogError("Level unlock not in range");
        }
    }

    public static bool LevelUnlocked(int level)
    {
        bool unlocked = false;

        if(level <= Application.levelCount - 1)
        {
            if(PlayerPrefs.HasKey(LEVEL_UNLOCK_KEY+level.ToString()) && PlayerPrefs.GetInt(LEVEL_UNLOCK_KEY + level.ToString()) == 1)
            {
                unlocked = true;
            }
        }
        else
        {
            Debug.LogError("Level unlock not in range");
        }

        return unlocked;
    }
}
