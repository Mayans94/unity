﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    #region Variables

    public float speed = 5.0f;
    public float padding = 1.0f;
    public GameObject projectile;
    public float projectileSpeed;
    public float fireRate;
    public float health;
    public AudioClip fire;

    float xMin;
    float xMax;
    float yMin;
    float yMax;
    float newX;
    float newY;

    #endregion

    #region Flow

    // Use this for initialization
    void Start ()
    {
        Cursor.visible = false;
        float distance = transform.position.z - Camera.main.transform.position.z;

        xMin = Camera.main.ViewportToWorldPoint(new Vector3(0, 0,distance)).x + padding;
        xMax = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, distance)).x - padding;
        yMin = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, distance)).y + padding;
        yMax = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, distance)).y - padding;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            InvokeRepeating("Fire", 0.00001f,fireRate);
        }
        
        if(Input.GetKeyUp(KeyCode.Space))
        {
            CancelInvoke("Fire");
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.position += Vector3.left * speed * Time.deltaTime;
        }
        else if(Input.GetKey(KeyCode.RightArrow))
        {
            transform.position += Vector3.right * speed * Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.position += Vector3.up * speed * Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.position += Vector3.down * speed * Time.deltaTime;
        }

        //Restrict Player to Gamespace
        newX = Mathf.Clamp(transform.position.x, xMin, xMax);
        newY = Mathf.Clamp(transform.position.y, yMin, yMax);
        transform.position = new Vector3(newX, newY);

    }

    void Fire()
    {
        Vector3 offSet = new Vector3(0, 0.8f, 0) + transform.position;
        GameObject shot = Instantiate(projectile, offSet, Quaternion.identity) as GameObject;
        shot.GetComponent<Rigidbody2D>().velocity = new Vector3(0, projectileSpeed, 0);
        AudioSource.PlayClipAtPoint(fire, transform.position);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Projectile shot = collision.gameObject.GetComponent<Projectile>();

        if (shot != null)
        {
            health -= shot.GetDamage();
            if (health <= 0)
            {
               Die();
            }

            shot.Hit();
        }
    }

    void Die()
    {
        Cursor.visible = true;
        LevelManager man = GameObject.Find("LevelManager").GetComponent<LevelManager>();
        man.LoadLevel("Win Screen");
        Destroy(gameObject);
    }
    #endregion

}
