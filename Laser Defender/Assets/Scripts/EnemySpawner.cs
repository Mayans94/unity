﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {

    #region Variables

    public GameObject enemyPrefab;
    public float width;
    public float height;
    public float speed;
    public float spawnDelay;

    private bool movingRight = true;
    private float xMin;
    private float xMax;
    private float newX;

    #endregion

    #region Flow

    // Use this for initialization
    void Start ()
    {
        float distance = transform.position.z - Camera.main.transform.position.z;

        xMin = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, distance)).x;
        xMax = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, distance)).x;

        SpawnUntilFull();
    }
	
	// Update is called once per frame
	void Update ()
    {
        EnemyFlow();
    }

    private void EnemyFlow()
    {
        if (movingRight)
        {
            transform.position += Vector3.right * speed * Time.deltaTime;
        }
        else
        {
            transform.position += Vector3.left * speed * Time.deltaTime;
        }


        float rightEdge = transform.position.x + (width * 0.5f);
        float leftEdge = transform.position.x - (width * 0.5f);

        if (leftEdge < xMin)
        {
            movingRight = true;
        }
        else if (rightEdge > xMax)
        {
            movingRight = false;
        }

        if(AllMembersDead())
        {
            SpawnUntilFull();
        }
    }

    Transform NextFreePosition()
    {
        foreach (Transform child in transform)
        {
            if (child.childCount == 0)
            {
                return child;
            }
        }

        return null;
    }

    private void SpawnUntilFull()
    {
        Transform freePos = NextFreePosition();

        if (freePos != null)
        {
            GameObject enemy = Instantiate(enemyPrefab, freePos.position, Quaternion.identity) as GameObject;
            enemy.transform.parent = freePos;
        }

        if (NextFreePosition() != null)
        {
            Invoke("SpawnUntilFull", spawnDelay);
        }
    }

    private bool AllMembersDead()
    {
        foreach(Transform child in transform)
        {
          if(child.childCount > 0)
            {
                return false;
            } 
        }

        return true;
    }

    private void LoadEnemies()
    {
        foreach (Transform child in transform)
        {
            GameObject enemy = Instantiate(enemyPrefab, child.transform.position, Quaternion.identity) as GameObject;
            enemy.transform.parent = child.transform;
        }

    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(transform.position, new Vector3(width, height));
    }

    #endregion
}
