﻿using UnityEngine;
using System.Collections;

public class MusicPlayer : MonoBehaviour {

    static MusicPlayer instance = null;
    private AudioSource music;

    public AudioClip startClip;
    public AudioClip endClip;
    public AudioClip gameClip;


    void Start ()
    {

		if (instance != null && instance != this)
        {
			Destroy (gameObject);
		}
        else
        {
			instance = this;
			GameObject.DontDestroyOnLoad(gameObject);
            music = GetComponent<AudioSource>();
            music.clip = startClip;
            music.loop = true;
            music.Play();
		}
		
	}

    private void OnLevelWasLoaded(int level)
    {
        music.Stop();

        switch(level)
        {
            case 0:
                {
                    music.clip = startClip;
                    break;
                }
            case 1:
                {
                    music.clip = gameClip;
                    break;
                }
            case 2:
                {
                    music.clip = endClip;
                    break;
                }
            default:
                {
                    music.clip = gameClip;
                    break;
                }
        }

        music.loop = true;
        music.Play();
    }
}
