﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour {

    #region Variables

    public float health;
    public GameObject enemyShot;
    public float speed;
    public float shotsPerSecond;
    public int scoreValue;
    public AudioClip fireSound;
    public AudioClip deathSound;

    private float probability;
    private ScoreKeeper keepScore;

    #endregion

    #region Flow

    // Use this for initialization
    void Start ()
    {
        keepScore = GameObject.Find("Score").GetComponent<ScoreKeeper>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        probability = Time.deltaTime * shotsPerSecond;

        if (Random.value < probability)
        {
            Fire();
        } 
	}

    private void Fire()
    {
        GameObject shot = Instantiate(enemyShot, transform.position, Quaternion.identity) as GameObject;
        shot.GetComponent<Rigidbody2D>().velocity = new Vector3(0, -speed, 0);
        AudioSource.PlayClipAtPoint(fireSound, transform.position);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Projectile shot = collision.gameObject.GetComponent<Projectile>();

        if (shot != null)
        {
            health -= shot.GetDamage();
            if(health <= 0)
            {
                Die();
            }

            shot.Hit();
        }
    }

    private void Die()
    {
        AudioSource.PlayClipAtPoint(deathSound, transform.position);
        Destroy(gameObject);
        keepScore.Score(scoreValue);
    }

    #endregion
}
