﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreKeeper : MonoBehaviour {

    #region Variables

    private Text myText;
    public static int score = 0;

    #endregion

    #region Flow

    // Use this for initialization
    void Start ()
    {
       myText = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void Score(int _points)
    {
        score += _points;
        myText.text = score.ToString();
    }

    public static void Reset()
    {
        score = 0;
    }

    #endregion
}
