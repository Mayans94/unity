﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManger : MonoBehaviour
{
    #region Flow

    public void LoadNextLevel()
    {
        Pill.pillCount = 0;
        if(Application.loadedLevelName == "Level_03")
        {
            Cursor.visible = true;
        }
        else
        {
            Cursor.visible = false;
        }
        
        Application.LoadLevel(Application.loadedLevel + 1);
    }

    public void LoadLevel(string name)
    {
        Pill.pillCount = 0;
        if (name == "Level_01")
        {
            Cursor.visible = false;
        }
        else
        {
            Cursor.visible = true;
        }

        SceneManager.LoadScene(name);
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void Cheat()
    {
        Paddle.autoPlay = true;
    }

    public void BrickDestroyed()
    {
        if(Pill.pillCount <= 0)
        {
            LoadNextLevel();
        }
    }

    #endregion
}
