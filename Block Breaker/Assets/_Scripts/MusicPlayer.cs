﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayer : MonoBehaviour {

    #region Variables

    static MusicPlayer instance = null;

    #endregion

    #region Flow

    // Use this for initialization
    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    void Start ()
    {
       
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    #endregion
}
