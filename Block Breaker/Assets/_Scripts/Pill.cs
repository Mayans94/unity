﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pill : MonoBehaviour {

    #region Variables

    public AudioClip pop;
    public AudioClip destroyed;
    private int maxHits = 1;
    private int timesHit;
    private LevelManger levelManager;
    public Sprite[] hitPills;
    private int spriteHit = 0;
    private float yPos;
    public float amplitude;
    public float speed;
    public static int pillCount = 0;
    private bool isBreakable;

    #endregion

    #region Flow

    // Use this for initialization
    void Start ()
    {
        levelManager = FindObjectOfType<LevelManger>();
        timesHit = 0;
        yPos = transform.position.y;

        isBreakable = (this.tag == "Breakable");

        if(isBreakable)
        {
            pillCount++;
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
        Bouncing();
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        timesHit++;
        maxHits = hitPills.Length + 1;

        if (timesHit >= maxHits)
        {
            pillCount--;
            levelManager.BrickDestroyed();
            AudioSource.PlayClipAtPoint(destroyed, transform.position);
            Destroy(gameObject);
        }
        else
        {
            AudioSource.PlayClipAtPoint(pop, transform.position);
            LoadSprites();
        }
    }

    private void LoadSprites()
    {
        spriteHit = timesHit - 1;
        this.GetComponent<SpriteRenderer>().sprite = hitPills[spriteHit];
    }

    private void Bouncing()
    {
        transform.position = new Vector3(transform.position.x,yPos + amplitude * Mathf.Sin(speed * Time.time));
    }

    #endregion
}
