﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class loseCollider : MonoBehaviour {

    #region Variables

    private LevelManger levelManager;

    #endregion

    #region Flow

    // Use this for initialization
    void Start ()
    {
        levelManager = GameObject.FindObjectOfType<LevelManger>();
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    #endregion

    #region Colliders

    void OnTriggerEnter2D(Collider2D collider)
    {
        Pill.pillCount = 0;
        levelManager.LoadLevel("Loose");
    }

    #endregion
}
