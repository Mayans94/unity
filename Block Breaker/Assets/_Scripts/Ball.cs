﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {

    #region Variables

    private Paddle paddle;
    private bool started = false;
    private Vector3 paddleToBallVector;
    private int counter = 0;
    public Sprite[] eating;

    #endregion

    #region Flow

    // Use this for initialization
    void Start ()
    {
        paddle = GameObject.FindObjectOfType<Paddle>();
        paddleToBallVector = this.transform.position - paddle.transform.position;
        InvokeRepeating("Eating", 0, 0.18f);
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (!started)
        {
            this.transform.position = paddle.transform.position + paddleToBallVector;

            if (Input.GetMouseButtonDown(0))
            {
                started = true;
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(2f, 10f);
            }
        }
	}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Vector2 tweak = new Vector2(Random.Range(0,0.2f), Random.Range(0, 0.2f));

        if (started)
        { 
            GetComponent<AudioSource>().Play();
            this.GetComponent<Rigidbody2D>().velocity += tweak; 
        }
    }

    private void Eating()
    {
        
        this.GetComponent<SpriteRenderer>().sprite = eating[counter];

        if (counter >= 3)
        {
            counter = 0;
        }

        counter++;
    }

    #endregion
}
